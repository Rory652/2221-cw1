import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Date;

/**
 * Responds to a client's request
 */
public class ClientHandler extends Thread {
    private Socket socket = null;       // The socket
    private PrintWriter out = null;     // Writes to the client (through the socket)
    private BufferedReader in = null;   // Reads from the client (through the socket)

    // Variables that need to be maintained across threads
    private volatile static int numLists = -1;          // Contains the total number of lists
    private volatile static int listLength = -1;        // Contains the length of each list
    private volatile static String[][] lists = null;    // Stores the lists
    private volatile static int[] listSize = null;      // Contains how many elements are in each list

    /**
     * Creates a ClientHandler class and initialises the volatile variables if that hasn't happened already
     *
     * @param socket The socket the client is using
     * @param numLists The total number of lists
     * @param listLength The length of each list
     */
    public ClientHandler(Socket socket, int numLists, int listLength) {
        super("ClientHandler");
        this.socket = socket;

        // Only define variables if this is the first time a thread is created
        if (ClientHandler.numLists == -1) {
            ClientHandler.numLists = numLists;
            ClientHandler.listLength = listLength;

            initArrays();
        }
    }

    /**
     * Prepares the volatile arrays
     */
    private void initArrays() {
        lists = new String[numLists][listLength];
        listSize = new int[numLists];

        // Makes sure each element starts at 0
        for (int i = 0; i < numLists; i++) {
            listSize[i] = 0;
        }
    }

    /**
     * Runs the socket -
     *      Gets the command
     *      Responds to that command
     */
    public void run() {
        try {
            // Make sure you can connect correctly
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            String[] command = getCommand();
            int pos = command.length;

            // Handle Response - different functions for each command
            switch (command[0]) {
                case "totals" : {
                    respondTotals();
                    break;
                }
                case "list" : {
                    respondList(Integer.parseInt(command[1]));
                    break;
                }
                case "join" : {
                    respondJoin(Integer.parseInt(command[1]), command[2]);
                    break;
                }
                default : {
                    // An erroneous request managed to be sent to the server
                    out.println("Bye.");
                }
            }

            // Log request (function is thread safe as it's synchronized)
            logRequest(command, pos);

            // Close everything
            out.close();
            in.close();
            socket.close();
        } catch (IOException e) {
            // Print any errors that get thrown
            e.printStackTrace();
        }
    }

    /**
     * Gets the command from the client
     *
     * @return An array of strings containing the command
     * @throws IOException In case the reading from the client fails
     */
    private String[] getCommand() throws IOException {
        String inputLine;
        String[] command = new String[3];
        int pos = 0;

        // Start the transmission
        out.println("next");

        // Inputs the whole command from the client
        // Finishes when it receives "done"
        while ((inputLine = in.readLine()) != null) {
            if (!inputLine.equals("done")) {
                command[pos] = inputLine;
                pos++;
                out.println("next");
            } else {
                break;
            }
        }

        return command;
    }

    /**
     * Responds to the total command
     */
    private void respondTotals() {
        out.println("There are " + numLists + " lists with of a maximum size of " + listLength + ".");
        // Go through each list and send its size to the client
        for (int i = 0; i < numLists; i++) {
            int correctI = i+1;
            out.println("List " + correctI + " has " + listSize[i] + " member(s).");
        }

        // End the communication
        out.println("Bye.");
    }

    /**
     * Responds to the list command
     *
     * @param selectedList Contains the list the client wants to see
     */
    private void respondList(int selectedList) {
        // Lists are shown 1 -> n but are stored 0 -> n-1 so you need to decrememnt
        selectedList--;

        // Check that list number selected is valid
        if (selectedList >= 0 && selectedList < numLists) {
            // Send each name in the list to client
            for (int i = 0; i < listSize[selectedList]; i++) {
                out.println(lists[selectedList][i]);
            }
        } else {
            out.println("Error: list doesn't exist");
        }

        // End the communication
        out.println("Bye.");
    }

    /**
     * Responds to the join command
     *
     * @param selectedList List the client wants to join
     * @param name The name of the client
     */
    private void respondJoin(int selectedList, String name) {
        // Same reason as in respondList()
        selectedList--;

        if (selectedList >= 0 && selectedList < numLists) {
            // Check if user can fit in the list and add them if possible
            if (listSize[selectedList] < listLength) {
                lists[selectedList][listSize[selectedList]] = name;
                listSize[selectedList]++;

                out.println("Success.");
            } else {
                out.println("Failed.");
            }
        } else {
            out.println("Error: list doesn't exist");
        }

        // End the communication
        out.println("Bye.");
    }

    /**
     * Logs the client command (along with a timestamp and IP) to a log file
     *
     * @param command Array containing the client's command
     * @param length Length of the command to write
     * @throws IOException In case the writing fails
     */
    private synchronized void logRequest(String[] command, int length) throws IOException {
        FileWriter log = new FileWriter("log.txt", true);

        // Get date (and time) and IP address
        Date date = new Date();
        InetAddress ip = socket.getInetAddress();

        String toLog = date + " | " + ip + " |";
        // Add command to log string
        for (int i = 0; i < length; i++) {
            toLog += " " + command[i];
        }

        toLog += "\n";

        // Write to file and close it
        log.write(toLog);
        log.close();
    }
}