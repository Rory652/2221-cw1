import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Creates and maintains the Executor server and creates the threads (ClientHandler) to handle all requests
 */
public class Server {

	/**
	 * Validates the command line arguments and creates the server
	 * Then accepts client requests and assigns them a thread
	 *
	 * @param args Command line arguments
	 * @throws IOException In case the server fails to be created
	 */
	public static void main(String[] args) throws IOException {
		// Server must have 2 arguments
		if (args.length != 2) {
			throw new IllegalArgumentException();
		}

		// Will throw error if these aren't valid integers
		int lists = Integer.parseInt(args[0]);
		int members = Integer.parseInt(args[1]);

		if (!(lists > 0 && members > 0)) {
			throw new IllegalArgumentException("Arguments should be > 0");
		}

		ServerSocket server = null;

		try {
			server = new ServerSocket(9652);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		// Create the executor server with a fixed thread pool
		ExecutorService service = Executors.newFixedThreadPool(25);

		// Delete existing log file (if possible)
		File log = new File("log.txt");
		log.delete();

		while (true) {
			// Wait for clients and respond to them
			Socket client = server.accept();
			service.submit(new ClientHandler(client, lists, members));
		}
	}
}