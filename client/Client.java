import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Objects;

/**
 *  Connects to the server and sends the request. Then outputs the server's response
 */
public class Client {
	private Socket socket;		// The socket
	private PrintWriter out;	// Reads from the server
	private BufferedReader in;	// Writes to the server

	/**
	 * Runs the client by connecting to a server, sending the command and printing the output
	 *
	 * @param command Contains the client's command
	 */
	private void run(String[] command) {
		try {
			// Try to connect to server
			socket = new Socket("localhost", 9652);

			// Setup the data streams
			out = new PrintWriter(socket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		String response;
		int pos = 0;
		boolean commandSent = false;

		try {
			// Communicate with server (ends when "Bye." is sent)
			while ((response = in.readLine()) != null) {
				if (response.equals("Bye.")) {
					break;
				} else if (pos == command.length && !commandSent) {
					// Client knows that it has send the whole command
					commandSent = true;
					out.println("done");
				} else if (response.equals("next")) {
					// Sends whole command (one argument at a time)
					out.println(command[pos]);
					pos++;
				} else {
					System.out.println(response);
				}
			}

			// Close everything
			out.close();
			in.close();
			socket.close();
		}
		catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * Validates the client's command line arguments for each of the commands
	 *
	 * @param args Command line arguments to be validated
	 * @return true if the arguments are valid, false otherwise
	 */
	public static boolean validateArgs(String[] args) {
		// Validate the arguments
		try {
			// Check each type of command
			if (args.length == 1 && args[0].equals("totals")) {
				return true;
			} else if (args.length == 2 && args[0].equals("list")) {
				Integer.parseInt(args[1]);
				return true;
			} else if (args.length == 3 && args[0].equals("join")) {
				Integer.parseInt(args[1]);
				if (args[2].length() == 0) {
					throw new IllegalArgumentException("Error: name length should be > 1");
				}
				return true;
			} else {
				throw new IllegalArgumentException("Error: incorrect number of arguments");
			}
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Error: number should be an integer");
		}
	}

	/**
	 * Validates the arguments then creates and runs the client
	 *
	 * @param args Command line arguments containing the request
	 */
	public static void main(String[] args) {
		if (validateArgs(args)) {
			Client client = new Client();
			client.run(args);
		}
	}
}